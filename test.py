import pickle
import threading

import cv2
import numpy as np
from matplotlib import pyplot as plt
from skimage.measure import label
from sklearn.tree import DecisionTreeClassifier

NA = 0
disagree = 0

with open('data.pickle', 'rb') as f:
    clf = pickle.load(f)

img = cv2.imread("symbols.png")
im_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
th, im_gray_th_otsu = cv2.threshold(im_gray, 254, 255, cv2.THRESH_OTSU)
labeled = label(im_gray_th_otsu)


def do_job(start, stop):
    for i in range(start, stop):
        wh = np.where(labeled == i)
        y_min = wh[0].min() - 1
        x_min = wh[1].min() - 1
        y_max = wh[0].max() + 2
        x_max = wh[1].max() + 2
        width = (x_max - x_min) - 2
        height = (y_max - y_min) - 2
        letter = im_gray_th_otsu[y_min:y_max, x_min:x_max]
        letter[letter == 0] = 1
        labeled2 = label(letter)
        holes = labeled2.max() - 2
        hole2 = len(np.where(labeled2 == 4)[0])
        hole1 = len(np.where(labeled2 == 3)[0])
        ratio = width / height

        if np.all(letter[1:-1, 1:2] == 255):
            line = 1
        else:
            line = 0
        # name = clf.predict([[line, ratio, hole1 / (width * height), hole2 / (width * height)]])
        name = ""
        if holes == 0:
            if line == 1:
                name = "-"
            elif line == 0:
                if 0.9 <= ratio <= 1.1:
                    name = "*"
                elif 0.55 <= ratio <= 0.65:
                    name = "1"
                elif 0.4 <= ratio <= 0.55:
                    name = "SLASH"
                elif 1.15 <= ratio <= 1.25:
                    name = "W"
                elif 0.7 <= ratio <= 0.8:
                    name = "X"
                else:
                    name = "N/A"
            else:
                name = "N/A"
        elif holes == 1:
            if line == 1:
                if hole1 / (width * height) < 0.3:
                    name = "P"
                else:
                    name = "D"
            elif line == 0:
                if hole1 / (width * height) < 0.15:
                    name = "A"
                else:
                    name = "0"
            else:
                name = "N/A"
        elif holes == 2:
            if line == 1:
                name = "B"
            elif line == 0:
                name = "8"
            else:
                name = "N/A"
        else:
            name = "N/A"

        lock = threading.Lock()
        if name == "N/A":
            with lock:
                global NA
                NA += 1

        name2 = clf.predict([[line, ratio, hole1 / (width * height), hole2 / (width * height)]])[0]

        if name != name2:
            print(f"DISAGREE. IMG: {i}")
            with lock:
                global disagree
                disagree += 1
        # print(f"{i/labeled.max() * 100}%")
        with lock:
            plt.title(str(i) + " " + name + " " + str(name2))
            plt.imshow(labeled2)
            plt.savefig(f"answers/{i}.png")


# threads = []
# N = 8
# for t in range(N):
#     threads.append(
#         threading.Thread(target=do_job, args=(t * (labeled.max() + 1) // N, (t + 1) * (labeled.max() + 1) // N)))
#
# for th in threads:
#     th.start()
#
# for th in threads:
#     th.join()

do_job(1, labeled.max() + 1)

print(f"Disagrees: {disagree}")
print(f"N/A: {NA}")