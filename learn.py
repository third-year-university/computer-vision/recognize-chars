import os
import cv2
import numpy as np
from matplotlib import pyplot as plt
from skimage.measure import label
from sklearn.tree import DecisionTreeClassifier
import graphviz
from sklearn import tree
import pickle


def get_perimetr(img):
    p = 0
    for i in range(1, img.shape[0] - 1):
        for j in range(1, img.shape[1] - 1):
            if (img[i - 1][j] == 255 or img[i + 1][j] == 255 or img[i][j - 1] == 255 or img[i][j + 1] == 255) \
                    and img[i][j + 1] == 0:
                p += 1
    return p


directory = 'alphabet'

clf = DecisionTreeClassifier()
Xs = []
Ys = []

for file_name in os.listdir(directory):
    img = cv2.imread(directory + "/" + file_name)
    im_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    th, im_gray_th_otsu = cv2.threshold(im_gray, 128, 255, cv2.THRESH_OTSU)
    labeled = label(im_gray_th_otsu)
    area = len(np.where(im_gray_th_otsu == 0)[0])
    wh = np.where(labeled == 0)
    y_min = wh[0].min()
    x_min = wh[1].min()
    y_max = wh[0].max() + 1
    x_max = wh[1].max() + 1
    perimetr = get_perimetr(im_gray_th_otsu)
    width = (x_max - x_min)
    height = (y_max - y_min)
    ratio = width / height
    holes = labeled.max() - 1
    letter = im_gray_th_otsu[y_min:y_max, x_min:x_max]
    # plt.imshow(letter[y_min:y_max, 0:1])
    # plt.show()
    if np.all(letter[y_min:y_max, 0:1] == 0):
        line = 1
    else:
        line = 0
    hole2 = len(np.where(labeled == 3)[0])
    hole1 = len(np.where(labeled == 2)[0])
    print([file_name[:-4], ratio, hole1 / (width * height), hole2 / (width * height), line])
    Xs.append([line, ratio, hole1 / (width * height), hole2 / (width * height)])
    Ys.append(file_name[:-4])

clf.fit(Xs, Ys)
dot_data = tree.export_graphviz(clf, out_file=None)
graph = graphviz.Source(dot_data)
graph.render("mytree")

with open('data.pickle', 'wb') as f:
    pickle.dump(clf, f)


